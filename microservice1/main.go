package main

import (
	"fmt"
	"log"
	"net/http"
)

/*
Первый микросервис - это простой HTTP сервер,
который будет слушать на порту 8080 и возвращать строку “Hello from microservice1” при обращении к корневому URL:
*/

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		_, err := fmt.Fprintf(w, "Hello from microservice 1")
		if err != nil {
			log.Fatal(err)
		}
	})
	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		log.Fatal(err)
	}
}
