package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"time"
)

/*
Второй микросервис - это клиент, который будет обращаться к первому микросервису и выводить полученную строку в консоль:
*/

func main() {
	for i := 0; i < 5; i++ {
		resp, err := http.Get("http://microservice1:8080")
		if err != nil {
			log.Fatal(err)
		}

		defer func(Body io.ReadCloser) {
			err := Body.Close()
			if err != nil {

			}
		}(resp.Body)

		body, err := io.ReadAll(resp.Body)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println(string(body))
		time.Sleep(1 * time.Second)
	}
}
